# Adaptive Monte Carlo Localization 

Please find the current code base in the `code/catkin_ws/src/particle_filter_localization_project` package.

The launch files etc can be found in the same package.

The main script is: `code/catkin_ws/src/particle_filter_localization_project/scripts/particle_filter.py`

My preliminary experiments related to the motion model can be found in the ipynbs in `code/independent_exp` folder.

The code was tested on the `Melodic` version of ROS in Ubuntu 18.04


## Some results and snapshots from the current implementation can be found in `Overview Slides.pdf`.

The simulation **(for the results)** can be run by running the below commands.


### Steps to run

Unzip the map files in `code/catkin_ws/src/particle_filter_localization_project/map`


```console
foo@bar:~$ roslaunch turtlebot3_gazebo turtlebot3_house.launch
foo@bar:~$ roslaunch particle_filter_project visualize_particles.launch
foo@bar:~$ roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
foo@bar:~$ rosrun particle_filter_project particle_filter.py
```

### The package is dependent on the following packages (please make sure these are present before running the code):
* turtlebot3
* turtlebot3_msgs
* turtlebot3_simulations