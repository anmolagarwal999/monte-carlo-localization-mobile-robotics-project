# CMake generated Testfile for 
# Source directory: /home/anmolagarwal/Desktop/mr_mcl_project/catkin_ws/src
# Build directory: /home/anmolagarwal/Desktop/mr_mcl_project/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("agitr")
subdirs("turtlebot3/turtlebot3")
subdirs("turtlebot3_msgs")
subdirs("turtlebot3/turtlebot3_navigation")
subdirs("turtlebot3_simulations/turtlebot3_simulations")
subdirs("udacity_bot")
subdirs("particle_filter_localization_project")
subdirs("turtlebot3/turtlebot3_bringup")
subdirs("turtlebot3/turtlebot3_example")
subdirs("turtlebot3_simulations/turtlebot3_fake")
subdirs("turtlebot3_simulations/turtlebot3_gazebo")
subdirs("turtlebot3/turtlebot3_slam")
subdirs("turtlebot3/turtlebot3_teleop")
subdirs("turtlebot3/turtlebot3_description")
