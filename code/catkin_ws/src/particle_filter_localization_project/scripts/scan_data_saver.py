import rospy 
import rospkg 
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState, GetModelState
import math
from sensor_msgs.msg import PointCloud2
import sys
import numpy as np
from sensor_msgs.msg import LaserScan
from copy import deepcopy
import pickle
import sensor_msgs.point_cloud2 as pc2
import laser_geometry.laser_geometry as lg

lp = lg.LaserProjection()


def set_model_state(x_c, y_c, theta_c_degrees):
    try:

        state_msg = ModelState()
        state_msg.model_name = 'turtlebot3'
        state_msg.pose.position.x = x_c
        state_msg.pose.position.y = y_c
        state_msg.pose.position.z = 0
        state_msg.pose.orientation.x = 0
        state_msg.pose.orientation.y = 0
        state_msg.pose.orientation.z = math.sin(math.radians(theta_c_degrees/2))
        state_msg.pose.orientation.w = math.cos(math.radians(theta_c_degrees/2))

        set_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
        resp = set_state( state_msg )

    except rospy.ServiceException as e:
        print ("Service call failed:",  e)
        
def get_model_state():
    try:
        model_name = 'turtlebot3'
        gms = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
        resp1 = gms(model_name,"map")
        return resp1

    except rospy.ServiceException as e:
        print ("Service call failed:",  e)

def are_poses_close(x_c, y_c, theta_c, curr_pose):
    # print(curr_pose.pose.position)
    # print(curr_pose.pose.orientation)
    # print(x_c, y_c, math.cos(math.radians(theta_c)), math.sin(math.radians(theta_c)))
    p1 = np.isclose(x_c, curr_pose.pose.position.x, atol=1e-03)
    p2 = np.isclose(y_c, curr_pose.pose.position.y, atol=1e-03)
    p3 = np.isclose(math.sin(math.radians(theta_c/2)), curr_pose.pose.orientation.z, atol=1e-03)
    # print("cmp is : ", math.sin(math.radians(theta_c)), curr_pose.pose.orientation.z)
    p4 = np.isclose(math.cos(math.radians(theta_c/2)), curr_pose.pose.orientation.w, atol=1e-03)
    
    # print(p1, p2, p3, p4)
    
    return p1 and p2 and p3 and p4

curr_scan = None
# id = True
def scan_cacher(msg):
    # print("Scan received is: ")
    assert(msg is not None)
    global curr_scan, found_scan
    # print("curr scan is ", curr_scan)
    curr_scan = deepcopy(msg)
    # found_scan = True
    # print("header is ", msg.header)
    assert(curr_scan is not None)

def main():
    global curr_scan, found_scan
    rospy.init_node('scan_data_saver')
    rospy.Subscriber("scan", LaserScan, scan_cacher)

    rospy.wait_for_service('/gazebo/set_model_state')
    rospy.wait_for_service('/gazebo/get_model_state')
    
    scan_arr  =[]
    
    # x_lb = -6.5
    
    extra  = 0.03
    x_lb = -2.5
    x_ub = 8.5
    
    y_lb = -2.2
    y_ub = 1.7
    
    theta_lb = 0
    theta_ub = 355
    # theta_ub = 1
    
    failed_arr = []
    
    prev_scan_id = -1
    
    for curr_x in np.arange(x_lb, x_ub+0.05, 0.2):
        for curr_y in np.arange(y_lb, y_ub+0.05, 0.2):
            for curr_theta in np.arange(theta_lb, theta_ub+1, 15):
                # set_model_state(0, 0, 0)
                # rospy.sleep(0.25)
                
                print(curr_x, curr_y, curr_theta)
                failed_attempts = 0
                while True:
                    
                    set_model_state(curr_x,curr_y, curr_theta)
                    ans = get_model_state()
                    prev_scan_id = curr_scan.header.seq if curr_scan is not None else -1
                    # print ("Are poses close ?")
                    is_close = are_poses_close(curr_x, curr_y, curr_theta, ans)
                    
                    if is_close:
                        # print("It is close")
                        while True:
                            curr_scan_id = curr_scan.header.seq if curr_scan is not None else -1
                            # print("prev ", prev_scan_id, "curr id is ", curr_scan_id)
                            if curr_scan_id==prev_scan_id:
                                continue
                            else:
                                break
                        break
                    else:
                        if failed_attempts==3:
                            break
                        else:
                            failed_attempts+=1
                    rospy.sleep(0.05)
                
                if failed_attempts!=3:
                    
                    assert(curr_scan is not None)
                    scan_arr.append({
                        "x_c": curr_x, 
                        "y_c":curr_y, 
                        "theta_c":curr_theta, 
                        "scan_msg":curr_scan,
                        "pcd_msg":lp.projectLaser(curr_scan)
                    })
                else:
                    print("Failed attempt")
                    failed_arr.append({curr_x, curr_y, curr_theta})
                print("########")
                    
                    
    
        with open('/home/anmolagarwal/Desktop/mounted_dump/less_granular_filename_main_wider_res_new_parths_map_corrected.pickle', 'wb') as handle:
            pickle.dump({"scan_arr":scan_arr, "failed_arr":failed_arr}, handle, protocol=pickle.HIGHEST_PROTOCOL)
                    
    
    # print "ans is "
    # print ans
    
    

    

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass