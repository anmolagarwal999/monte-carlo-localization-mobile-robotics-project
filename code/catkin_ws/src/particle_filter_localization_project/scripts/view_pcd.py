import open3d as o3d
import numpy as np
import matplotlib.pyplot as plt
import copy
import os
import sys
import numpy as np
from copy import deepcopy
# import helper
import seaborn as sns

# path="./results/Q1/pcds/between_boat_and_car.pcd"
path_1="/home/anmolagarwal/Desktop/mr_mcl_project/analysis_based_stuff/got.pcd"
pcd_1 = o3d.io.read_point_cloud(path_1)

path_2="/home/anmolagarwal/Desktop/mr_mcl_project/analysis_based_stuff/original.pcd"
pcd_2 = o3d.io.read_point_cloud(path_2)

R = pcd_2.get_rotation_matrix_from_xyz((0, 0, -np.pi / 2))
pcd_2.rotate(R, center=(0, 0, 0))

o3d.visualization.draw([pcd_1, pcd_2], show_ui=True)
