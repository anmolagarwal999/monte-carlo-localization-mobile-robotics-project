import rospy 
import rospkg 
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState
import numpy as np
import rospy 
import rospkg 
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState, GetModelState
import math

def get_model_state():
    try:
        model_name = 'turtlebot3'
        gms = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
        resp1 = gms(model_name,"map")
        print("resp is ", resp1)
        return resp1

    except rospy.ServiceException as e:
        print ("Service call failed:",  e)

def main():
    rospy.init_node('model_state_fetcher')
    rospy.wait_for_service('/gazebo/get_model_state')

    # state_msg = ModelState()
    # state_msg.model_name = 'turtlebot3'
    # state_msg.pose.position.x = 0.4
    # state_msg.pose.position.y = 0.4
    # state_msg.pose.position.z = 0
    # state_msg.pose.orientation.x = 0
    # state_msg.pose.orientation.y = 0
    # angle_deg = 180
    # state_msg.pose.orientation.z = np.sin(np.radians(angle_deg/2))
    # state_msg.pose.orientation.w = np.cos(np.radians(angle_deg/2))

    # rospy.wait_for_service('/gazebo/set_model_state')
    # try:
    #     set_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
    #     resp = set_state( state_msg )

    # except rospy.ServiceException as e:
    #     print ("Service call failed:",  e)
    while True:
        ans = get_model_state()
        rospy.sleep(1)

if __name__ == '__main__':
    try:
        main()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass