import sensor_msgs.point_cloud2 as pc2
import rospy
from sensor_msgs.msg import PointCloud2, LaserScan
import laser_geometry.laser_geometry as lg
import math
import json
from sklearn.neighbors import KDTree
import rosbag
import matplotlib.pyplot as plt
import pickle
import open3d as o3d
import numpy as np
import copy
import json

class estimate_traj:
    
    def __init__(self):
        pass
    

def fetch_icp_scores(db_pcd, query_pcd):
    
    trans_init = np.asarray([[1, 0, 0, 0], [0, 1, 0,0], [0,0,1,0],[0.0, 0.0, 0.0, 1.0]])
    threshold = 0.5
    
    
    
    target = query_pcd
    source = db_pcd
    
    
    
    reg_p2p = o3d.pipelines.registration.registration_icp(
        source, target, threshold, trans_init,
        o3d.pipelines.registration.TransformationEstimationPointToPoint())
    # print(reg_p2p)
    #print("Transformation is:")
    #print(reg_p2p.transformation)
    evaluation = o3d.pipelines.registration.evaluate_registration(
    source, target, 20, reg_p2p.transformation)
    # print(evaluation)
    
    ans = [reg_p2p, evaluation]
    # scores_arr.append([idx, reg_p2p, evaluation])
    #draw_registration_result(source, target, reg_p2p.transformation)
    return ans
    # print("#######################")
    #break
    
def fetch_best_contenders_2(query_db, curr_pcd,  min_x, max_x, min_y, max_y):
    
    scores_arr = []
    
    for idx, curr_elem in enumerate(query_db):
        
        x, y, theta = curr_elem['x'], curr_elem['y'], curr_elem['theta']
        
        if x>max_x or x<min_x:
            continue
        
        if y>max_y or y<min_y:
            continue
    
        icp_ans = fetch_icp_scores(curr_elem['pcd_obj'], curr_pcd['pcd_obj'])
        scores_arr.append((idx, icp_ans[0], icp_ans[1]))
    return scores_arr
    
def chamfer_distance_sklearn(array1,array2):
    batch_size, num_point = array1.shape[:2]
    print("Batch size is ", batch_size)
    print("Num points is ", num_point)
    dist = 0
    for i in range(batch_size):
        tree1 = KDTree(array1[i], leaf_size=num_point+1)
        tree2 = KDTree(array2[i], leaf_size=num_point+1)
        distances1, _ = tree1.query(array2[i])
        distances2, _ = tree2.query(array1[i])
        av_dist1 = np.mean(distances1)
        av_dist2 = np.mean(distances2)
        dist = dist + (av_dist1+av_dist2)/batch_size
    return dist

def fetch_best_contenders(query_db, curr_pcd,  min_x, max_x, min_y, max_y):
    
    scores_arr = []
    tree1 = KDTree(np.asarray(curr_pcd['pcd_obj'].points))  
    
    for idx, curr_elem in enumerate(query_db):
        
        x, y, theta = curr_elem['x'], curr_elem['y'], curr_elem['theta']
        
        if x>max_x or x<min_x:
            continue
        
        if y>max_y or y<min_y:
            continue
    
        tree2 =  KDTree(np.asarray(curr_elem['pcd_obj'].points)) 
        
        distances1, _ = tree1.query(np.asarray(curr_elem['pcd_obj'].points))
        distances2, _ = tree2.query(np.asarray(curr_pcd['pcd_obj'].points))
        
        av_dist1 = np.mean(distances1)
        av_dist2 = np.mean(distances2)
        
        scores_arr.append([idx, av_dist1+av_dist2])
        # icp_ans = fetch_icp_scores(curr_elem['pcd_obj'], curr_pcd['pcd_obj'])
        # scores_arr.append((idx, icp_ans[0], icp_ans[1]))
    return scores_arr