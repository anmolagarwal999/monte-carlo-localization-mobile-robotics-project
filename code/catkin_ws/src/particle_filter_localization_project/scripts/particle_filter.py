#!/home/anmolagarwal/miniconda3/envs/mr_mcl/bin/python

import sensor_utils.sensor_stuff as ss_helper
import rospy


from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Quaternion, Point, Pose, PoseArray, PoseStamped
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Header, String
import pickle

import tf
from tf import TransformListener
from tf import TransformBroadcaster
from tf.transformations import quaternion_from_euler, euler_from_quaternion
from copy import deepcopy
import numpy as np
from numpy.random import random_sample
import math

from random import randint, random, uniform
###########################
import sys

import rospy
import os

from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Quaternion, Point, Pose, PoseArray, PoseStamped
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Header, String

import numpy as np
from numpy.random import random_sample
import math
import sensor_msgs.point_cloud2 as pc2
import json
from random import randint, random

import sensor_msgs.point_cloud2 as pc2
import rospy
from sensor_msgs.msg import PointCloud2, LaserScan
import laser_geometry.laser_geometry as lg
import math
import rosbag
import matplotlib.pyplot as plt
import pickle
import sensor_utils.sensor_stuff as sensor_helper


reload(sys)
sys.setdefaultencoding('utf8')
####################


def get_yaw_from_pose(p):
    """ A helper function that takes in a Pose object (geometry_msgs) and returns yaw"""

    yaw = (euler_from_quaternion([
        p.orientation.x,
        p.orientation.y,
        p.orientation.z,
        p.orientation.w])
        [2])

    return yaw


def draw_random_sample():
    """ Draws a random sample of n elements from a given list of choices and their specified probabilities.
    We recommend that you fill in this function using random_sample.
    """
    # TODO
    return


alpha_1 = 0.01
alpha_2 = 0.01
alpha_3 = 0.01
alpha_4 = 0.01


def quaternion_to_euler_angle_vectorized(w, x, y, z):
    ysqr = y * y
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (ysqr + z * z)
    Z = np.degrees(np.arctan2(t3, t4))
    Z = np.arctan2(t3, t4)

    return Z


def sample_motion_model_odometry(prev_loc, current_loc, hyp_loc, print_stuff=False):
    dx = current_loc[0] - prev_loc[0]
    dy = current_loc[1] - prev_loc[1]
    dtheta = current_loc[2] - prev_loc[2]

    drot1 = math.atan2(dy, dx) - prev_loc[2]
    dtrans = math.sqrt(dx*dx+dy*dy)
    drot2 = dtheta - drot1

    if print_stuff:
        print("Stats are: ")
        print("Initial rotation 1 (deg) is: ", np.degrees(drot1), drot1)
        print("Init translation is: ", dtrans)
        print("Initial rotation 2 (deg) is ", np.degrees(drot2), drot2)

    var1 = alpha_1*math.fabs(drot1) + alpha_2*math.fabs(dtrans)
    std1 = math.sqrt(var1)
    #std1 = var1

    var2 = alpha_3*math.fabs(dtrans) + alpha_4 * math.fabs((drot1+drot2))
    std2 = math.sqrt(var2)
    #std2 = var2

    var3 = alpha_1*math.fabs(drot2) + alpha_2 * math.fabs(dtrans)
    std3 = math.sqrt(var3)
    #std3 = var3

    if print_stuff:
        print("vars are ", var1, var2, var3)
        print("Std dev are ", std1, std2, std3)

    #dr1 = np.random.normal(0, std1, 1)[0]
    #dt = np.random.normal(0, std2, 1)[0]
    #dr2 = np.random.normal(0, std3, 1)[0]

    dr1 = std1 * np.random.normal(0, 1, 1)[0]
    dt = std2 * np.random.normal(0, 1, 1)[0]
    dr2 = std3 * np.random.normal(0, 1, 1)[0]

    drot1_actual = drot1 - dr1
    dtrans_actual = dtrans - dt
    drot2_actual = drot2 - dr2

    if print_stuff:
        print("dr1 degrees ", np.degrees(dr1), dr1)
        print("dt  ", dt)
        print("dr2 degrees ", np.degrees(dr2), dr2)

    if print_stuff:
        print("Stats are: ")
        print("Rot 1: degrees: ", np.degrees(drot1), np.degrees(drot1_actual))
        print(dtrans, dtrans_actual)
        print("Rot 2: degrees: ", np.degrees(drot2), np.degrees(drot2_actual))

    ans_x = hyp_loc[0] + dtrans_actual * math.cos(hyp_loc[2]+drot1_actual)
    ans_y = hyp_loc[1] + dtrans_actual * math.sin(hyp_loc[2]+drot1_actual)
    ans_theta = hyp_loc[2] + drot1_actual + drot2_actual
    return [ans_x, ans_y, ans_theta]


class ParticleFilter:

    def __init__(self):

        # once everything is setup initialized will be set to true
        self.initialized = False

        # initialize this particle filter node
        # You can only have one node in a rospy process, so you can only call rospy.init_node() once.
        print("Initializing the node")
        rospy.init_node('turtlebot3_particle_filter')

        #TODO: Checkout
        # set the topic names and frame names
        print("initializing the frame names")
        self.base_frame = "base_footprint"
        self.map_topic = "map"
        self.odom_frame = "odom"
        self.scan_topic = "scan"

        #TODO: Checkout
        # inialize our map
        self.map = OccupancyGrid()
        print("Attributes in occupancy grid is: ", self.map)

        # the number of particles used in the particle filter
        # TODO: Reincrease to a larger number
        # self.num_particles = 5
        # print("Starting with particles number: ", self.num_particles)

        # initialize the particle cloud array
        self.particle_cloud = []

        # TODO: checkout the arguments
        # initialize the estimated robot pose
        self.robot_estimate = Pose()

        # set threshold values for linear and angular movement before we preform an update
        self.lin_mvmt_threshold = 0.2
        self.ang_mvmt_threshold = (np.pi / 6)

        self.odom_pose_last_motion_update = None

        #########################################################################################
        # Setup publishers and subscribers

        '''Publishers'''

        # publisher for the current particle cloud
        self.particles_pub = rospy.Publisher(
            "particle_cloud", PoseArray, queue_size=10)

        # publisher for the estimated robot pose
        self.robot_estimate_pub = rospy.Publisher(
            "estimated_robot_pose", PoseStamped, queue_size=10)

        '''Subscribers'''
        # subscribe to the map server
        rospy.Subscriber(self.map_topic, OccupancyGrid, self.get_map)

        # subscribe to the lidar scan from the robot
        rospy.Subscriber(self.scan_topic, LaserScan, self.robot_scan_received)

        #############################################################################################

        # enable listening for and broadcasting corodinate transforms
        self.tf_listener = TransformListener()
        self.tf_broadcaster = TransformBroadcaster()

        ###########################################################################################
        # intialize the particle cloud
        self.initialize_particle_cloud()
        self.initialized = True
        self.publish_particle_cloud()

        self.cnt = 0

        ###########################################
        self.map_obj = sensor_helper.load_map()
        self.grid_obj = sensor_helper.fetch_map_grid(self.map_obj)
        self.lf_obj = sensor_helper.LikelihoodField(self.map_obj)

    def get_map(self, data):
        # data is an occupancy grid
        print("Subscription from topic: [ ", self.map_topic, " ]")
        self.map = data

    def initialize_particle_cloud(self):

        # TODO: Fill particles here
        # initially, all particles should have same weight ie 1
        for i in np.arange(-5, 1, 0.7):
            for j in np.arange(-1, 3, 0.7):
                init_pose = Pose()
                init_pose.position.x = i
                init_pose.position.y = j
                init_pose.position.z = 0
                init_pose.orientation.x = 0
                init_pose.orientation.y = 0
                init_pose.orientation.z = 0
                init_pose.orientation.w = 1
                self.particle_cloud.append(ss_helper.Particle(
                    pose=init_pose,
                    w=1.0))

        # initially, the weights will be same
        self.normalize_particles()

        self.publish_particle_cloud()

    def normalize_particles(self):
        # TODO: make all the particle weights sum to 1.0
        print("Normalization started")
        tot_sum = sum([x.w for x in self.particle_cloud])
        for curr_particle in self.particle_cloud:
            curr_particle.w /= tot_sum
        print("Normalization done")

        pass

    def publish_particle_cloud(self):
        print("Publishing particle cloud")
        particle_cloud_pose_array = PoseArray()

        # the particle cloud is expected in map frame
        particle_cloud_pose_array.header = Header(
            stamp=rospy.Time.now(), frame_id=self.map_topic)
        particle_cloud_pose_array.poses

        for part in self.particle_cloud:
            particle_cloud_pose_array.poses.append(part.pose)
        print("Number points is: ", len(particle_cloud_pose_array.poses))
        self.particles_pub.publish(particle_cloud_pose_array)

    def publish_estimated_robot_pose(self):

        robot_pose_estimate_stamped = PoseStamped()
        robot_pose_estimate_stamped.pose = self.robot_estimate
        robot_pose_estimate_stamped.header = Header(
            stamp=rospy.Time.now(), frame_id=self.map_topic)
        self.robot_estimate_pub.publish(robot_pose_estimate_stamped)

    def resample_particles(self):
        print("NOW RESAMPLING")
        num_particles = len(self.particle_cloud)
        num_cum = [0]*num_particles

        self.new_particle_cloud = []

        curr_val = 0
        for idx, curr_particle in enumerate(self.particle_cloud):
            curr_val += curr_particle.w
            num_cum[idx] = curr_val

        for i in range(len(self.particle_cloud)):
            rand_num = uniform(0, 1)
            assert (rand_num <= num_cum[-1])
            idx_to_choose = -1
            for idx, elem in enumerate(num_cum):
                if elem >= rand_num:
                    idx_to_choose = idx
                    break
            print("Choosing ", idx_to_choose)
            self.new_particle_cloud.append(deepcopy(self.particle_cloud[idx_to_choose]))
            
        # print("Num sum is ", num_cum)
        # TODO


        self.particle_cloud = self.new_particle_cloud
        self.particle_cloud = sorted(self.particle_cloud, key=lambda x: -x.w)
        self.print_particle_cloud()
        print("##########################")
        pass

    def robot_scan_received(self, data):
        '''Receives laser scan data'''

        # print("Subscription from topic: [ ", self.scan_topic, " ]")
        # print("Laser scan data is: ")
        # print(data)

        #################
        # TODO: DEBUG: Remove
        # if self.cnt%20==0:
        #     print("Counter is ", self.cnt)
        #     self.publish_particle_cloud()
        # self.cnt+=1
        ###################

        # print("##############################")

        #############################################################################################
        # wait until initialization is complete
        if not (self.initialized):
            print("Self init not yet complete")
            return

        # we need to be able to transfrom the laser frame to the base frame
        # The canTransform() methods return a bool whether the transform can be evaluated.
        # Test if source_frame can be transformed to target_frame at time 'time'.
        # base_frame = base_footprint
        # header.frame_id = base_scan
        if not (self.tf_listener.canTransform(self.base_frame, data.header.frame_id, data.header.stamp)):
            return

        # wait for a little bit for the transform to become avaliable (in case the scan arrives
        # a little bit before the odom to base_footprint transform was updated)
        self.tf_listener.waitForTransform(
            self.base_frame, self.odom_frame, data.header.stamp, rospy.Duration(0.5))
        if not (self.tf_listener.canTransform(self.base_frame, data.header.frame_id, data.header.stamp)):
            return
        #####################################################################################################

        # calculate the pose of the laser distance sensor
        # 'p' is initialized with frame_id = base_scan
        p = PoseStamped(
            header=Header(stamp=rospy.Time(0),
                          frame_id=data.header.frame_id))
        # https://w3.cs.jmu.edu/spragunr/CS354_S14/labs/tf_lab/html/tf.listener.TransformerROS-class.html#transformPose
        # transform the pose "p" which was measured in frame "p.header.frame_id" to frame "self.base_frame"
        # Since, we did not supply any pose, "p" is iniitialized with location and angles as "0".
        # this makes sense as in frame of "scanner", the "scanner will be located at origin itself"
        self.laser_pose = self.tf_listener.transformPose(self.base_frame, p)
        # print("self laser pose is : ", self.laser_pose)

        ##################################################################################################

        # determine where the robot thinks it is based on its odometry
        p = PoseStamped(
            header=Header(stamp=data.header.stamp,
                          frame_id=self.base_frame),
            pose=Pose())

        self.odom_pose = self.tf_listener.transformPose(self.odom_frame, p)
        # print("self odom pose is : ", self.odom_pose)

        ############################################################################################
        # we need to be able to compare the current odom pose to the prior odom pose
        # if there isn't a prior odom pose, set the odom_pose variable to the current pose
        if not self.odom_pose_last_motion_update:
            self.odom_pose_last_motion_update = self.odom_pose
            return

        #########################################################################################

        if self.particle_cloud:

            # check to see if we've moved far enough to perform an update

            curr_x = self.odom_pose.pose.position.x
            old_x = self.odom_pose_last_motion_update.pose.position.x
            curr_y = self.odom_pose.pose.position.y
            old_y = self.odom_pose_last_motion_update.pose.position.y
            curr_yaw = get_yaw_from_pose(self.odom_pose.pose)
            old_yaw = get_yaw_from_pose(self.odom_pose_last_motion_update.pose)

            if (np.abs(curr_x - old_x) > self.lin_mvmt_threshold or
                np.abs(curr_y - old_y) > self.lin_mvmt_threshold or
                    np.abs(curr_yaw - old_yaw) > self.ang_mvmt_threshold):

                print("It's time for an update")
                print("diff values are: ")
                print("dx: ", curr_x-old_x)
                print("dy: ", curr_y-old_y)
                print("dyaw: ", curr_yaw-old_yaw)

                print("last motion update is ",
                      self.odom_pose_last_motion_update)
                # with open('sample_prev_loc.pickle', 'wb') as handle:
                #     pickle.dump(self.odom_pose_last_motion_update,
                #                 handle, protocol=pickle.HIGHEST_PROTOCOL)

                # with open('sample_curr_loc.pickle', 'wb') as handle:
                #     pickle.dump(self.odom_pose, handle,
                #                 protocol=pickle.HIGHEST_PROTOCOL)
                print("Current motion is ", self.odom_pose)

                print("\n\n###############################################\n\n")
                # This is where the main logic of the particle filter is carried out

                ################
                # done
                self.update_particles_with_motion_model()
                #################
                ####################
                self.update_particle_weights_with_measurement_model(data)
                #################
                ####################
                # done
                self.normalize_particles()
                #################
                ####################
                self.resample_particles()
                #################
                ####################
                self.update_estimated_robot_pose()
                #################
                ####################
                self.publish_particle_cloud()
                #################
                ####################
                self.publish_estimated_robot_pose()
                #################
                ####################
                self.odom_pose_last_motion_update = self.odom_pose
                #################
                ####################
                
    def print_particle_cloud(self):
        for idx, curr_particle in enumerate(self.particle_cloud):
            x, y = curr_particle.pose.position.x, curr_particle.pose.position.y
            print("Particle at ", idx, " has coords: ", x, y, " and w: ", curr_particle.w)

    def update_estimated_robot_pose(self):
        # based on the particles within the particle cloud, update the robot pose estimate

        # for the time being, crudely select the pose with the maximum probability
        self.robot_estimate = self.particle_cloud[0].pose
        print("Estimated being published is: ", self.robot_estimate)

        # TODO
        pass

    def update_particle_weights_with_measurement_model(self, data_obj):
        print("Inside the function update_particle_weights_with_measurement_model() ")

        # print("STarting writing data")
        # with open('data.pickle', 'wb') as handle:
        #     pickle.dump({"data":data}, handle, protocol=2)
        # print("STORED AND WRITTEN DATA")
        # print("DATA IS ", data_obj)
        scan_pcd = sensor_helper.fetch_pcd_from_scan(data_obj)
        BEAM_GAP_USE = 20
        scan_pcd = list(filter(lambda x:(x['index']%BEAM_GAP_USE)==0, scan_pcd))

        # iterate through all points
        for idx, curr_particle in enumerate(self.particle_cloud):
            # print(curr_particle)
            print("particle id: ", idx)
            # plt.imshow(grid_obj)
            curr_particle.w = sensor_helper.fetch_particle_probability(
                curr_particle, scan_pcd, self.lf_obj, beam_gap=BEAM_GAP_USE)
            # break
        self.particle_cloud = sorted(self.particle_cloud, key=lambda x: -x.w)
        print("TOP 10 are:")
        print([(x.w, x.pose.position.x, x.pose.position.y)
              for x in self.particle_cloud[:6]])
        print("#################")
        self.print_particle_cloud()
        # TODO
        pass

    @staticmethod
    def fetch_short_coordinates(pose_obj):
        try:
            ans = [pose_obj.position.x, pose_obj.position.y, 0]
            Z = quaternion_to_euler_angle_vectorized(
                pose_obj.orientation.w,
                pose_obj.orientation.x,
                pose_obj.orientation.y,
                pose_obj.orientation.z
            )
            ans[2] = Z
        except:
            return ParticleFilter.fetch_short_coordinates(pose_obj.pose)

        return ans

    @staticmethod
    def fetch_new_pose(org_pose, new_short):
        org_pose.position.x = new_short[0]
        org_pose.position.y = new_short[1]

        org_pose.orientation.x = 0
        org_pose.orientation.y = 0
        org_pose.orientation.z = np.sin(new_short[2]/2)
        org_pose.orientation.w = np.cos(new_short[2]/2)
        return org_pose

    def update_particles_with_motion_model(self):

        # based on the how the robot has moved (calculated from its odometry), we'll  move
        # all of the particles correspondingly
        print("Upating particles based on motion model")
        prev_loc = self.fetch_short_coordinates(
            self.odom_pose_last_motion_update)
        curr_loc = self.fetch_short_coordinates(self.odom_pose)

        for curr_particle in self.particle_cloud:
            hyp_loc = self.fetch_short_coordinates(curr_particle.pose)
            ans_loc = sample_motion_model_odometry(prev_loc, curr_loc, hyp_loc)
            curr_particle.pose = self.fetch_new_pose(
                curr_particle.pose, ans_loc)
            print("From: (", hyp_loc[0], hyp_loc[1], ") | we got : (",curr_particle.pose.position.x,curr_particle.pose.position.y )
        print("Update based on motion model done")
        self.print_particle_cloud()
        # TODO
        pass


if __name__ == "__main__":
    pf = ParticleFilter()
    print("Class initialized")
    pf.publish_particle_cloud()
    print("Going to start spinning")
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()
